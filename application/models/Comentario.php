<?php
/**
 *
 */
class Comentario extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function guardar($data){
    $this->db->insert('comentario', $data);
    return $this->db->insert_id();
  }
  public function obtener_tablas(){
    $comentarios=$this->db->get('comentario');
    if ($comentarios->num_rows()>0) {
        return $comentarios;
      } else {
        return false; //cuando no hay datos

  }
}
public function eliminarPorId($id_com){
  $this->db->where("id_com", $id_com);
          return $this->db->delete("comentario");
}
public function ObtenerPorId($id_com){
  $this->db->where("id_com",$id_com);
  $destinos=$this->db->get("comentario");
  if ($destinos->num_rows()>0){
    return $destinos->row();
  } else {
    return false;
  }

}
  public function actualizar($id_game,$data){
    $this->db->where("id_game",$id_game);
    return $this->db->update("game",$data);
  }

}
