<?php
/**
 *
 */
class Actividad extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function guardar($data){
    $this->db->insert('actividad', $data);
    return $this->db->insert_id();
  }
  public function obtener_tablas(){
    $actividades=$this->db->get('actividad');
    if ($actividades->num_rows()>0) {
        return $actividades;
      } else {
        return false; //cuando no hay datos

  }
}
public function eliminarPorId($id_act){
  $this->db->where("id_act", $id_act);
          return $this->db->delete("actividad");
}
public function ObtenerPorId($id_act){
  $this->db->where("id_act",$id_act);
  $destinos=$this->db->get("actividad");
  if ($destinos->num_rows()>0){
    return $destinos->row();
  } else {
    return false;
  }

}
  public function actualizar($id_game,$data){
    $this->db->where("id_game",$id_game);
    return $this->db->update("game",$data);
  }

}
