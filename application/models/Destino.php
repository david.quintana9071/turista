<?php
/**
 *
 */
class Destino extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function guardar($data){
    $this->db->insert('destino', $data);
    return $this->db->insert_id();
  }
  public function obtener_tablas(){
    $destinos=$this->db->get('destino');
    if ($destinos->num_rows()>0) {
        return $destinos;
      } else {
        return false; //cuando no hay datos

  }
}
public function eliminarPorId($id_des){
  $this->db->where("id_des", $id_des);
          return $this->db->delete("destino");
}
public function ObtenerPorId($id_des){
  $this->db->where("id_des",$id_des);
  $destinos=$this->db->get("destino");
  if ($destinos->num_rows()>0){
    return $destinos->row();
  } else {
    return false;
  }

}
  public function actualizar($id_game,$data){
    $this->db->where("id_game",$id_game);
    return $this->db->update("game",$data);
  }

}
