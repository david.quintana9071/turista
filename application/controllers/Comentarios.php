<?php

/**
 *
 */
class Comentarios extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Comentario');
  }
  public function index(){
    $data['listadoComentario'] = $this->Comentario->obtener_tablas();
    $this->load->view('header');
    $this->load->view('comentarios/index',$data);
    $this->load->view('footer');

  }
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('comentarios/nuevo');
    $this->load->view('footer');

  }
  public function guardar()
{
  $data = array(
    'lugar_com' => $this->input->post('lugar_com'),
    'cliente_com' => $this->input->post('cliente_com'),
    'comentario' => $this->input->post('comentario'),
    'recomendaciones' => $this->input->post('recomendaciones')
  );

  print_r($data);
  if ($this->Comentario->guardar($data)){
    redirect('comentarios/index');
  }else {
    echo "<h1>ERROR AL GUARDAR</h1>";
  }
}
public function borrar($id_com){
  if ($this->Comentario->eliminarPorId($id_com)){
    redirect('comentarios/index');
  }else{
      echo "Error al eliminar :(";
  }
}

public function actualizar($id_game){
		$data["gameEditar"]=$this->Game->ObtenerPorId($id_game);
		$this->load->view("header");
		$this->load->view("games/actualizar",$data);
		$this->load->view("footer");
	}
	//Funcion para procesar botón actualización
	public function actualizacion(){
		$datosGameEditado=array(
      "nombre_game"=>$this->input->post('nombre_game'),
			"genero_game"=>$this->input->post('genero_game'),
			"descripcion_game"=>$this->input->post('descripcion_game'),
			"puntuacion_game"=>$this->input->post('puntuacion_game'),
			"modo_game"=>$this->input->post('modo_game')
		);
		$id_game=$this->input->post("id_game");
		if ($this->Game->actualizar($id_game,$datosGameEditado)) {
			redirect('games/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}
