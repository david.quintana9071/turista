<?php

/**
 *
 */
class Destinos extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Destino');
  }
  public function index(){
    $data['listadoDestinos'] = $this->Destino->obtener_tablas();
    $this->load->view('header');
    $this->load->view('destinos/index',$data);
    $this->load->view('footer');

  }
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('destinos/nuevo');
    $this->load->view('footer');

  }
  public function guardar()
{
  $data = array(
    'nombre_des' => $this->input->post('nombre_des'),
    'ubicacion_des' => $this->input->post('ubicacion_des'),
    'atracciones_des' => $this->input->post('atracciones_des'),
    'clima_des' => $this->input->post('clima_des'),
    'descripcion_des' => $this->input->post('descripcion_des'),
  );

  print_r($data);
  if ($this->Destino->guardar($data)){
    redirect('destinos/index');
  }else {
    echo "<h1>ERROR AL GUARDAR</h1>";
  }
}
public function borrar($id_des){
  if ($this->Destino->eliminarPorId($id_des)){
    redirect('destinos/index');
  }else{
      echo "Error al eliminar :(";
  }
}

public function actualizar($id_game){
		$data["gameEditar"]=$this->Game->ObtenerPorId($id_game);
		$this->load->view("header");
		$this->load->view("games/actualizar",$data);
		$this->load->view("footer");
	}
	//Funcion para procesar botón actualización
	public function actualizacion(){
		$datosGameEditado=array(
      "nombre_game"=>$this->input->post('nombre_game'),
			"genero_game"=>$this->input->post('genero_game'),
			"descripcion_game"=>$this->input->post('descripcion_game'),
			"puntuacion_game"=>$this->input->post('puntuacion_game'),
			"modo_game"=>$this->input->post('modo_game')
		);
		$id_game=$this->input->post("id_game");
		if ($this->Game->actualizar($id_game,$datosGameEditado)) {
			redirect('games/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}
