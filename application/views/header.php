<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Turis Ecuador</title>
    <link rel="icon" href="<?php echo base_url('assets/icon/favicon.ico'); ?>" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
    <img src="<?php echo base_url();?>/assets/images/turista.png" height="100px" width="1950px">
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url(); ?>">Da click TURISTA</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url('nosotros/index'); ?>">Nosotros</a></li>
        <li class="dropdown">
          <a href="<?php echo site_url('destinos/index'); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Destino<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('destinos/index'); ?>">Listado</a></li>
            <li><a href="<?php echo site_url('destinos/nuevo'); ?>">Nuevo</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="<?php echo site_url('actividades/index'); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actividades<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('actividades/index'); ?>">Listado</a></li>
            <li><a href="<?php echo site_url('actividades/nuevo'); ?>">Nuevo</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="<?php echo site_url('comentarios/index'); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Comentario<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('comentarios/index'); ?>">Listado</a></li>
            <li><a href="<?php echo site_url('comentarios/nuevo'); ?>">Nuevo</a></li>
          </ul>
        </li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  </body>
</html>
