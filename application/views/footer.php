<footer class="footer mt-auto py-3" style="background-color: #333333; color: #FFFFFF;">
  <div class="container text-center">
    <br>
    <span class="text-muted">¡Bienvenidos a los juegos!</span>
    <br>
    <span class="text-muted">GAME YOU¡¡</span>
    <br>
    <span class="text-muted">QUITO-MACHACHI</span>
    <br>
    <span class="text-muted">¡Bienvenidos a los juegos!</span>
    <br>
    <div class="row">
      <div class="col-md-4">
        <h5>Enlaces útiles</h5>
        <ul class="list-unstyled">
          <li><a href="#">Inicio</a></li>
          <li><a href="#">Juegos</a></li>
          <li><a href="#">Noticias</a></li>
          <li><a href="#">Contacto</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <h5>Redes sociales</h5>
        <ul class="list-unstyled">
          <li><a href="#"><i class="fab fa-facebook-f"></i> Facebook</a></li>
          <li><a href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
          <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
          <li><a href="#"><i class="fab fa-youtube"></i> YouTube</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <h5>Contacto</h5>
        <p>Teléfono: 123-456-789</p>
        <p>Email: info@tuempresa.com</p>
        <p>Dirección: Quito, Ecuador</p>
      </div>
    </div>
    <br>
    <p class="text-muted">&copy; <?php echo date('Y'); ?> Tu Sitio Web. Todos los derechos reservados.</p>
  </div>
</footer>
