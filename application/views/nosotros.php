<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <title>Home</title>
    <style>
    body {
      backgroun-image: url('<?php echo base_url("assets/images/quitso.png"); ?>');
      background-repeat: no-repeat;
      background-size: cover;

    }

    </style>
  </head>
  <body>
    <div>
      <center> <h1>MISIÓN:</h1>
      <h2>Conducir el desarrollo turístico nacional, mediante las actividades de planeación, impulso al desarrollo de la oferta, apoyo a la operación de los servicios turísticos y la promoción, articulando las acciones de diferentes instancias y niveles de gobierno.</h2> </center>

    </div>
    <div>
      <center> <h1>VISIÓN:</h1>
      <h2>Ser país líder en la actividad turística en el 2030.

    Logrando:

        Reconocer al turismo como pieza clave del desarrollo económico de México.
        Diversificar los productos turísticos y desarrollar nuevos mercados.
        Impulsar a las empresas turísticas a ser competitivas a nivel nacional e internacional.
        Desarrollar el turismo respetando los entornos naturales, culturales y sociales.
    </h2> </center>

    </div>

  </body>
</html>
