<legend class="text-center">
  <i class="glyphicon glyphicon-user"></i><b> Listado de Destinos</b>
  <hr>
  <center>
  <a href="<?php echo site_url('destinos/nuevo');?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo</a>
  </center>
  <br>
  <br>
</legend>
<hr>
<?php if ($listadoDestinos): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">DESTINO</th>
        <th class="text-center">Ubicación</th>
        <th class="text-center">Atracciones</th>
        <th class="text-center">Clima</th>
        <th class="text-center">Descripción</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoDestinos->result() as $proveedorTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $proveedorTemporal->id_des; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->nombre_des; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->ubicacion_des; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->atracciones_des; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->clima_des; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->descripcion_des; ?></td>
          <td class="text-center">
            <!--<a href="<?php echo site_url('/actualizar'); ?>/<?php echo $proveedorTemporal->id_des; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i>Editar</a> -->
            <a href="<?php echo site_url('destinos/borrar'); ?>/<?php echo $proveedorTemporal->id_des; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3 class="text-center"><b>No existen Destinos</b></h3>
<?php endif; ?>
<br><br><br><br><br><br>
