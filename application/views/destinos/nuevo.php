<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  NUEVO DESTINO
</legend>
<br><br>
<form class="col-md-12" action="<?php echo site_url('destinos/guardar'); ?>" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">Nombre del destino:</label>
      <br>
      <input type="text" name="nombre_des" value="" class="form-control" id="nombre_des" placeholder="Ingrese el nombre del lugar" required>

    </div>
    <div class="col-md-4">
      <label for="">Ubicación:</label>
      <br>
      <input type="text" name="ubicacion_des" value="" class="form-control" id="ubicacion_des" placeholder="Ingrese la ubicación" required>

    </div>
    <div class="col-md-4">
      <label for="">Atracciones:</label>
      <br>
      <input type="text" name="atracciones_des" value="" class="form-control" id="atracciones_des" placeholder="Ingrese las atracciones" required>

    </div>
    <div class="col-md-4">
      <label for="">Clima:</label>
      <br>
      <input type="text" name="clima_des" value="" class="form-control" id="clima_des" placeholder="Ingrese el clima" required>

    </div>
    <div class="col-md-4">
      <label for="">Descripción:</label>
      <br>
      <input type="text" name="descripcion_des" value="" class="form-control" id="descripcion_des" placeholder="Ingrese la descripción" required>

    </div>

  </div>
  <br><br><br><br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    &nbsp;
    <a href="<?php echo site_url('destinos/index'); ?>" class="btn btn-danger">CANCELAR</a>

  </div>

</form>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
