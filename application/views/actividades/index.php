<legend class="text-center">
  <i class="glyphicon glyphicon-user"></i><b> Listado de Actividades</b>
  <hr>
  <center>
  <a href="<?php echo site_url('actividades/nuevo');?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo</a>
  </center>
  <br>
  <br>
</legend>
<hr>
<?php if ($listadoActividad): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">ACTIVIDAD</th>
        <th class="text-center">DESCRIPCIÓN</th>
        <th class="text-center">DURACIÓN</th>
        <th class="text-center">NIVEL</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoActividad->result() as $proveedorTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $proveedorTemporal->id_act; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->nombre_act; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->descripcion_act; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->duracion_act; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->nivel_act; ?></td>
          <td class="text-center">
            <!--<a href="<?php echo site_url('/actualizar'); ?>/<?php echo $proveedorTemporal->id_des; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i>Editar</a> -->
            <a href="<?php echo site_url('actividades/borrar'); ?>/<?php echo $proveedorTemporal->id_act; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3 class="text-center"><b>No existen Actividades</b></h3>
<?php endif; ?>
<br><br><br><br><br><br>
