<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  NUEVA ACTIVIDAD
</legend>
<br><br>
<form class="col-md-12" action="<?php echo site_url('actividades/guardar'); ?>" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">Actividad</label>
      <br>
      <input type="text" name="nombre_act" value="" class="form-control" id="nombre_act" placeholder="Ingrese la actividad" required>

    </div>
    <div class="col-md-4">
      <label for="">Descripción</label>
      <br>
      <input type="text" name="descripcion_act" value="" class="form-control" id="descripcion_act" placeholder="Ingrese la descripción" required>

    </div>
    <div class="col-md-4">
      <label for="">Duración</label>
      <br>
      <input type="number" name="duracion_act" value="" class="form-control" id="duracion_act" placeholder="Ingrese la duración de la actividad" required>

    </div>
    <div class="col-md-4">
      <label for="">Nivel</label>
      <br>
      <input type="text" name="nivel_act" value="" class="form-control" id="nivel_act" placeholder="Ingrese el nivel" required>

    </div>


  </div>
  <br><br><br><br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    &nbsp;
    <a href="<?php echo site_url('actividades/index'); ?>" class="btn btn-danger">CANCELAR</a>

  </div>

</form>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
