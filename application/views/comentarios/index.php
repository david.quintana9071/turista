<legend class="text-center">
  <i class="glyphicon glyphicon-user"></i><b> Listado de Comentarios</b>
  <hr>
  <center>
  <a href="<?php echo site_url('comentarios/nuevo');?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo</a>
  </center>
  <br>
  <br>
</legend>
<hr>
<?php if ($listadoComentario): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">Opinion del lugar</th>
        <th class="text-center">COmentario</th>
        <th class="text-center">Publico</th>
        <th class="text-center">Recomendaciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoComentario->result() as $proveedorTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $proveedorTemporal->id_com; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->lugar_com; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->cliente_com; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->comentario; ?></td>
          <td class="text-center"><?php echo $proveedorTemporal->recomendaciones; ?></td>
          <td class="text-center">
            <!--<a href="<?php echo site_url('/actualizar'); ?>/<?php echo $proveedorTemporal->id_des; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i>Editar</a> -->
            <a href="<?php echo site_url('comentarios/borrar'); ?>/<?php echo $proveedorTemporal->id_com; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3 class="text-center"><b>No existen Comentarios</b></h3>
<?php endif; ?>
<br><br><br><br><br><br>
