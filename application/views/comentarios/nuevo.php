<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  NUEVO COMENTARIO
</legend>
<br><br>
<form class="col-md-12" action="<?php echo site_url('comentarios/guardar'); ?>" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">Comentario del lugar:</label>
      <br>
      <input type="text" name="lugar_com" value="" class="form-control" id="lugar_com" placeholder="Ingrese el nombre del lugar" required>

    </div>
    <div class="col-md-4">
      <label for="">Cliente comentario:</label>
      <br>
      <input type="text" name="cliente_com" value="" class="form-control" id="cliente_com" placeholder="Ingrese la ubicación" required>

    </div>
    <div class="col-md-4">
      <label for="">Comentarios de tus compañeros:</label>
      <br>
      <input type="text" name="comentario" value="" class="form-control" id="comentario" placeholder="Ingrese las atracciones" required>

    </div>
    <div class="col-md-4">
      <label for="">Recomendaciones:</label>
      <br>
      <input type="text" name="recomendaciones" value="" class="form-control" id="recomendaciones" placeholder="Ingrese el clima" required>

    </div>


  </div>
  <br><br><br><br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    &nbsp;
    <a href="<?php echo site_url('comentarios/index'); ?>" class="btn btn-danger">CANCELAR</a>

  </div>

</form>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
